export const environment = {
  production: true,
  envName: 'local',
  keycloak: {
    // Url of the Identity Provider
    issuer: 'http://wdnvmhcl011.markham.ca:8180/auth/',

    // Realm
    realm: 'markham',

    // The SPA's id. 
    // The SPA is registerd with this id at the auth-serverß
    clientId: 'my-angular-client',
  }
};